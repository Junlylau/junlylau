let game = new ex.Engine
(
    {
        width: 800,
        height: 600,
        canvasElementId: 'base',
        displayMode: ex.DisplayMode.Fixed,
    }
)



let MainMenu = ex.Scene.extend
(
    {
        // start-up logic, called once
        onInitialize: function(game){},
        //each time the scene is activated by Engine.goToScene
        onActivate: function()
        {
            let textwords =  new ex.UIActor();
            textwords.color = ex.Color.White;
            textwords.draw = function()
            {
                var ctx = document.getElementById('base').getContext('2d');
                ctx.font = '48px serif';
                ctx.fillStyle = "#FF0000"
                ctx.fillText('Changed Scene to Main Menu', 10, 50);
            }
            textwords.draw();
            this.add(textwords);

            let dummy = new ex.Actor(100,100,20,20);
            dummy.Color = ex.Color.Black;
            this.add(dummy);
            //why is dummy not showing???????
        },
        //each time the scene is deactivated by Engine.goToScene
        onDeactivate: function(){}
    }
);

class Square extends ex.Actor
{
    update(engine,delta)
    {
        if (game.input.keyboard.isHeld(ex.Input.Keys.Down) || game.input.keyboard.wasPressed(ex.Input.Keys.W))
        {
            square.y -=10
        }
        if (game.input.keyboard.wasPressed(ex.Input.Keys.Down) || game.input.keyboard.wasPressed(ex.Input.Keys.S))
        {
            square.y +=10
        }
        if (game.input.keyboard.wasPressed(ex.Input.Keys.Right) || game.input.keyboard.wasPressed(ex.Input.Keys.D))
        {
            square.x +=10
        }
        if (game.input.keyboard.wasPressed(ex.Input.Keys.Left) || game.input.keyboard.wasPressed(ex.Input.Keys.A))
        {
            square.x -=10
        }
        if (game.input.keyboard.wasPressed(ex.Input.Keys.Esc))
        {
            game.goToScene('mainmenu');
        }
    }
}

//problem, game will start on 'root' scene always?? how do i make it start with MY scene.
// let GameScreen = ex.Scene.extend
// (
//     {
//         // start-up logic, called once.
//         onInitialize: function(game)
//         {
//             let square = new Square(game.drawWidth/2, game.drawHeight/2, 20, 20);
//             square.color = ex.Color.Red;
//         },
//         //each time the scene is activated by Engine.goToScene
//         onActivate:function()
//         {
//
//         },
//         //each time the scene is deactivated by Engine.goToScene
//         onDeactivate: function(){}
//     }
// )
let square = new Square(game.drawWidth/2,game.drawHeight/2,20,20);
square.color = ex.Color.Red;
game.add(square);
game.addScene('mainmenu', new MainMenu());
// game.addScene('gamescreen', new GameScreen());
game.start();
// game.goToScene('gamescreen');